# OpenML dataset: Emissions-by-Cars

https://www.openml.org/d/43540

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This Data set contains various information about a set of cars that were manufactured with set of factory parameters like cylinder size, number of cylinders, fuel consumption, Carbon dioxide emissions etc.
Content
Feature set includes model, make, vehicle type, engine size,Transmission, fuel consumption etc. The target set is Carbon dioxide Emission. 
Inspiration
The inspiration was to learn and help understand the concept behind data science's basics.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43540) of an [OpenML dataset](https://www.openml.org/d/43540). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43540/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43540/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43540/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

